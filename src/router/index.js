import Vue from 'vue'
import Router from 'vue-router'
import {globalStore} from '@/globalStore'
import Hello from '@/components/Hello'
import Graphs from '@/components/Graphs'
import Admins from '@/components/Admins'
import AdminsCreate from '@/components/AdminsCreate'
import AdminsEdit from '@/components/AdminsEdit'
import Discounts from '@/components/Discounts'
import DiscountsCreate from '@/components/DiscountsCreate'
import DiscountsEdit from '@/components/DiscountsEdit'
import Plans from '@/components/Plans'
import PlansCreate from '@/components/PlansCreate'
import PlansEdit from '@/components/PlansEdit'
import Codes from '@/components/Codes'
import CodesGenerate from '@/components/CodesGenerate'
import CodesRecall from '@/components/CodesRecall'
import Tags from '@/components/Tags'
import TagsCreate from '@/components/TagsCreate'
import TagsRemove from '@/components/TagsRemove'
import Settings from '@/components/Settings'

Vue.use(Router)

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Hello',
			component: Hello,
		},
		{
			path: '/graphs',
			name: 'Graphs',
			component: Graphs,
		},
		{
			path: '/admins',
			name: 'Admins',
			component: Admins,
			children: [
				{
					path: 'create',
					component: AdminsCreate,

				},
				{
					path: 'edit',
					component: AdminsEdit,
				},
			],
			// redirect: '/admins/create'
		},
		{
			path: '/plans',
			name: 'Plans',
			component: Plans,
			children: [
				{
					path: 'create',
					component: PlansCreate,
				},
				{
					path: 'edit',
					component: PlansEdit,
				},
			]
		},
		{
			path: '/discounts',
			name: 'Discounts',
			component: Discounts,
			children: [
				{
					path: 'create',
					component: DiscountsCreate,
				},
				{
					path: 'edit',
					component: DiscountsEdit,
				},
			]
		},
		{
			path: '/codes',
			name: 'Codes',
			component: Codes,
			children: [
				{
					path: 'generate',
					component: CodesGenerate,
				},
				{
					path: 'recall',
					component: CodesRecall,
				},
			]
		},
		{
			path: '/tags',
			name: 'Tags',
			component: Tags,
			children: [
				{
					path: 'create',
					component: TagsCreate,
				},
				{
					path: 'remove',
					component: TagsRemove,
				},
			]
		},
		{
			path: '/settings',
			name: 'Settings',
			component: Settings,
		},
	]
})

router.beforeEach((to, from, next) => {
	console.log('before each:', to.path)
	let processed
	switch (to.path) {
		case '/':
			processed = 'Home'
			break
		case '/admins/':
			//no links to this route so shouldn't be needed
		case '/admins/create':
		case '/admins/edit':
			processed = 'Managing Admins'
			break
		case '/plans/':
		case '/plans/create':
		case '/plans/edit':
			processed = 'Managing Plans'
			break
		case '/discounts/':
		case '/discounts/create':
		case '/discounts/edit':
			processed = 'Manging Discounts'
			break
		case '/codes/':
		case '/codes/generate':
		case '/codes/recall':
			processed = 'Managing Discount Codes'
			break
		case '/graphs':
			processed = 'Graphs'
			break
		case '/tags/':
		case '/tags/create':
		case '/tags/remove':
			processed = 'Tags'
			break
		case '/settings':
			processed = 'Settings'
			break
	}
	globalStore.title = processed
	next()
})

export default router